// Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
// DOM - это объектнаямодель докуммента. То, что строит браузер, когда выводит страницу

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let newArr = arr.map(function (value) {
  return [`<li> ${value} </li>`];
});

let str = (`<ul> ${newArr.join("")} </ul>`);

let elem = document.getElementById("str");
elem.insertAdjacentHTML("beforeend", str);
